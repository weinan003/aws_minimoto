# COMP9243 Assignment 3

**Luke Tsekouras, Vincent Tran**

## How to use Minimoto

Simply run the minimoto client against a folder full of images.

    src/minimoto_client.py folder/full/of/images

The client will upload your images and begin processing your request. A link will be printed to the console, where you will be able to find your completed video. 

## Operations

There are several scripts in the `inf/` folder which are used to manipulate instances of the system. 

### Bringing up a cloud

This will deploy a cloud which will pull in changes from the specified branch whenever an instance is brought up. If the cloud exists already it will update the stack template, but note that this will not always update the instances within. 

    inf/create-cloud.sh <stack_name> <branch_name>

### Upgrading a cloud

In order to see freshly pushed changes in a cloud, the easiest thing to do is to terminate the affected instances via the console. 

A more hacky but faster way to achieve an upgrade is to run the following command on a given instance:

    /opt/aws/bin/cfn-init -v --stack <stack_name> --resource <config_name> --region ap-southeast-2

`<config_name>` is either `WatchdogLaunchConfig` or `EncoderLaunchConfig`, depending on the type of the instance. 

### Deleting a cloud

This will permanently delete a cloud and all its related resources

    inf/delete-cloud.sh <stack_name>

### Locating a cloud's resources

The client needs to know where the queue and input/output buckets are in order to function. This information is stored in the output parameters of the given stack, which can be retrieved using the following command:

    aws cloudformation describe-stack --stack-name <stack_name>