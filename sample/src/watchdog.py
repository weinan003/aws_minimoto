#!/usr/bin/python

from boto.ec2 import autoscale
from boto import sqs

import common

RELEVANT_ATTRIBUTES = [
    'ApproximateNumberOfMessages',
    'ApproximateNumberOfMessagesNotVisible',
    'ApproximateNumberOfMessagesDelayed',
]

SECONDS_PER_JOB = 90
SECONDS_DEADLINE = 300

logger = common.get_logger('Watchdog')
logger.info('Watchdog is running...')

# Determine how many jobs are currently in the queue
sqs_connection = sqs.connect_to_region(common.get_region())

queue = sqs_connection.get_queue(common.get_config()['JobQueueName'])
attributes = sqs_connection.get_queue_attributes(queue)

total_jobs = 0
for attribute in RELEVANT_ATTRIBUTES:
    total_jobs += int(attributes[attribute])

logger.info('Discovered %d jobs', total_jobs)

# Calculate how many instances we need to meet our deadline
desired_instances = 1 + (total_jobs * SECONDS_PER_JOB) / SECONDS_DEADLINE
desired_instances = min(desired_instances, common.get_config()['MaxInstances'])

# Gather the current status of the ASG
autoscale_connection = autoscale.connect_to_region(common.get_region())

asg = autoscale_connection.get_all_groups(names = [common.get_config()['EncoderGroupName']])[0]
instances = [ instance for instance in asg.instances if instance.lifecycle_state is not 'Terminating' ]
live_instances = len(instances)

logger.info('Discovered %d live instances', live_instances)

for instance in instances:
    if live_instances <= desired_instances: break

    status = common.get_work_status(instance.instance_id)

    if status == 'Idle':
        logger.info('Terminating instance %s (%s)', instance.instance_id, status)

        # Occasionally this may fail, if the autoscaling group attempts to terminate instances in between when we
        # counted them and now. We may try to terminate the last instance in a group, resulting in an error.
        autoscale_connection.terminate_instance(instance.instance_id)
        live_instances -= 1

    else:
        logger.info('Ignoring instance %s (%s)', instance.instance_id, status)

# Set the desired capacity on the encoder group
if desired_instances > live_instances:

    logger.info('Adjusting to %d instances', desired_instances)

    autoscale_connection.set_desired_capacity(
        group_name = common.get_config()['EncoderGroupName'],
        desired_capacity = desired_instances
    )

logger.info('Watchdog complete.')
