#!/usr/bin/python
import json, os, common, subprocess, errno
from boto import s3

logger = common.get_logger(__name__)
logger.info('Encoder has initialised.')

(sqs_connection, queue) = common.get_sqs_connection_and_queue(common.get_config()['JobQueueName'])

s3_connection = s3.connect_to_region(common.get_region())
input_bucket = s3_connection.get_bucket(common.get_config()['InputBucketName'])
output_bucket = s3_connection.get_bucket(common.get_config()['OutputBucketName'])

SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__)) + "/../inf/encoder.sh"

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def process_job():

    try:
        messages = []

        # Continuously long-poll for a message from SQS
        while len(messages) == 0:
            messages = sqs_connection.receive_message(queue)

        common.set_work_status(common.get_instance_id(), 'Working')

        message = messages[0]
        message_info = json.loads(message.get_body())
        logger.info('Got job: %s', message_info.get('id'))

        logger.info('Downloading images')
        DOWNLOAD_DIR = '/tmp/encoder_download_directory/'  + message_info.get('id')
        WORK_DIR = '/tmp/encoder_work_directory/' + message_info.get('id')
        OUTPUT_DIR = '/tmp/encoder_output_directory/' + message_info.get('id')

        mkdir_p(DOWNLOAD_DIR)
        mkdir_p(WORK_DIR)
        mkdir_p(OUTPUT_DIR)

        os.chdir(DOWNLOAD_DIR)
        for key in input_bucket.list(prefix = message_info.get('id')):
            logger.info('Downloading %s', key)
            key.get_contents_to_filename(key.name.replace('/', '_'))


        logger.info('Processing video')

        OUTPUT_FILE = OUTPUT_DIR + "/output.mp4"
        # print subprocess.call(['time', 'sh', SCRIPT_PATH, DOWNLOAD_DIR, WORK_DIR, OUTPUT_FILE])

        if subprocess.call(['sh', SCRIPT_PATH, DOWNLOAD_DIR, WORK_DIR, OUTPUT_FILE]) != 0:
            raise Exception

        logger.info('Uploading video to bucket')
        output_key = s3.key.Key(output_bucket)
        output_key.name = message_info.get('id') + "/output.mp4"
        if  output_key.set_contents_from_filename(OUTPUT_FILE) != os.stat(OUTPUT_FILE).st_size:
            raise Exception

        logger.info('Output file successfully uploading')

        sqs_connection.delete_message(queue, message)
        logger.info('Deleted job from queue')

        common.set_work_status(common.get_instance_id(), 'Idle')

        logger.info('Deleting files from S3 bucket')
        for key in input_bucket.list(prefix = message_info.get('id')):
            logger.info('Deleting %s', key)
            key.delete()

        logger.info('Successfully deleted all files from S3')

    except Exception:

        try:
            logger.exception('An error occurred while processing job %s', message)

            # Make the job visible again after 10 seconds
            sqs_connection.change_message_visibility(queue, message.receipt_handle, 10)
            common.set_work_status(common.get_instance_id(), 'Idle')

        except Exception:
            # Just to make sure we don't accidentally kill the encoder process, we capture any exceptions here
            pass

# Continuously process jobs
while True:
    process_job()
