#!/usr/bin/python
import sys, os, json, uuid
from boto import s3, sqs, cloudformation

if len(sys.argv) < 2:
    sys.stderr.write("Usage: ./minimoto_client.py <image_directory>\n")
    sys.exit(1)

image_directory = sys.argv[1]
uuid = uuid.uuid4()

# These credentials point to a generated account which only has permissions to the resources the client needs
CREDENTIALS = {
    'aws_access_key_id': 'AKIAJKKKVNOWBQUQBRIA',
    'aws_secret_access_key': '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'
}
REGION = 'ap-southeast-2'
STACK_NAME = 'minimoto'

cloudformation_connection = cloudformation.connect_to_region(REGION, **CREDENTIALS)
stack = cloudformation_connection.describe_stacks(stack_name_or_id = STACK_NAME)[0]

stack_info = dict()
for output in stack.outputs:
    stack_info[output.key] = output.value

print 'Client has initialised.'

print 'Connecting to S3'
s3_connection = s3.connect_to_region(REGION, **CREDENTIALS)

print 'Uploading images to bucket'
print stack_info['InputBucketName']
input_bucket = s3_connection.get_bucket(stack_info['InputBucketName'], validate = False)
for image in os.listdir(image_directory):
    print 'Uploading ' + image + ' to bucket'
    image_path = os.path.join(image_directory, image)
    key = s3.key.Key(input_bucket)
    key.name = str(uuid) + "/" + image
    if  key.set_contents_from_filename(image_path) != os.stat(image_path).st_size:
        sys.stderr.write("Error writing file to bucket\n")
        exit(1)

print 'Images uploaded successfully'

print 'Connecting to job queue'

sqs_connection = sqs.connect_to_region(REGION, **CREDENTIALS)
queue = sqs_connection.get_queue(stack_info['JobQueueName'])
queue.set_message_class(sqs.message.RawMessage)

print 'Writing message to job queue'
message = sqs.message.RawMessage()
message_body = {"id": str(uuid), "name": image_directory}
message.set_body(json.dumps(message_body))
queue.write(message)

print "Successfully wrote to the job queue"

print "Output file will be found at the following URL within 10 minutes:"
print "http://" + stack_info['OutputBucketName'] + ".s3.amazonaws.com/" + str(uuid) + "/output.mp4"
