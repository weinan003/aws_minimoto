import json, sys, logging
from boto import utils, ec2, sqs

def get_logger(name):

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler('/home/ec2-user/encoder.log')
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)

    def handle_exception(exc_type, exc_value, exc_traceback):

        if issubclass(exc_type, KeyboardInterrupt):
            return sys.__excepthook__(exc_type, exc_value, exc_traceback)

        logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))

    sys.excepthook = handle_exception

    return logger


def get_region():
    return utils.get_instance_identity()['document']['region']

def get_instance_id():
    return utils.get_instance_identity()['document']['instanceId']

def get_config():
    with open('/etc/config.json') as config_file:
        return json.load(config_file)

def get_work_status(instance_id):
    conn = ec2.connect_to_region(get_region())
    return conn.get_only_instances(instance_ids = [instance_id])[0].tags.get('WorkStatus')

def set_work_status(instance_id, work_status):
    conn = ec2.connect_to_region(get_region())
    conn.create_tags(
        resource_ids = [instance_id],
        tags = { 'WorkStatus': work_status }
    )

def get_sqs_connection_and_queue(name):

    sqs_connection = sqs.connect_to_region(get_region())
    queue = sqs_connection.get_queue(name)
    queue.set_message_class(sqs.message.RawMessage)

    return (sqs_connection, queue)
