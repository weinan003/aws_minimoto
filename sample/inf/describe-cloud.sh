#!/bin/sh

STACK_NAME=$1

if [ $# -lt 1 ]; then
    echo "ERROR: Missing STACK_NAME"
    echo "Usage: ./describe-cloud.sh STACK_NAME"
    exit 1
fi

aws cloudformation describe-stacks --stack-name "$STACK_NAME"
