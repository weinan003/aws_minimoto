#!/bin/sh

DIR_NAME=$( dirname $0 )
STACK_NAME=$1
BRANCH_NAME=$2

if [ $# -lt 1 ]; then
    echo "ERROR: Missing STACK_NAME"
    echo "Usage: ./create-cloud.sh STACK_NAME [ BRANCH_NAME ]"
    exit 1
fi

if aws cloudformation describe-stacks --stack-name "$STACK_NAME"; then
    ACTION=update-stack
else
    ACTION=create-stack
fi

if [ ! -z $2 ]; then
    PARAMETERS="--parameters ParameterKey=CodeBranch,ParameterValue='$BRANCH_NAME',UsePreviousValue=false"
fi

aws cloudformation "$ACTION" \
    --stack-name "$STACK_NAME" \
    --template-body "file://$DIR_NAME/cloud-template.json" \
    --capabilities CAPABILITY_IAM \
    $PARAMETERS
