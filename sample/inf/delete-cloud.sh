#!/bin/sh

STACK_NAME=$1

if [ $# -lt 1 ]; then
    echo "ERROR: Missing STACK_NAME"
    echo "Usage: ./delete-cloud.sh STACK_NAME"
    exit 1
fi

aws cloudformation delete-stack --stack-name "$STACK_NAME"
