import os
import boto3
import time
import datetime

MEASURETIME = 10

EC2Tag = "workingTag"
AMI_ID = 'ami-96666ff5'
REGION = "ap-southeast-2"
ENCODER_INSTANCE_TYPE = "t2.small"
OTHER_INSTANCE_TYPE = "t2.small"

def parse_tag(tag):
    return {kv["Key"]:kv["Value"] for kv in tag}

def scp(localpath,remotepath,addr,PEM):
    str = "scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i "\
         + PEM +" "+ localpath + " ubuntu@"+ addr +":" + remotepath\
         + " > /dev/null 2>&1"
    return os.system(str)

def chmod(remotepath,addr,PEM):
    str = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i "\
         + PEM + ' ubuntu@'+ addr + " chmod 777 " + remotepath
    return os.system(str)

def ssh_do(remotepath,addr,PEM):
    str = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i "\
         + PEM + ' ubuntu@' + addr +" " + remotepath\
         + " > /dev/null 2>&1"
    return os.system(str)

def upload_files_to_ec2(dns, PEM):
    time.sleep(5)
    while scp("./*.*", "~/", dns, PEM) != 0:
        time.sleep(5)
    scp("./*", "~/", dns, PEM)
    scp(PEM, "~/", dns, PEM)

def open_ssh_port(ec2, sg_id):
    try:
        '''add SSH port to the security group it belongs to '''
        sg_cli = ec2.SecurityGroup(sg_id)
        sg_cli.authorize_ingress(
            IpProtocol = "tcp",
            FromPort = 22,
            ToPort = 22,
            CidrIp = "0.0.0.0/0"
        )
    except:
        pass




def create_ec2(ec2, tag_value, insType, PEM_path):
    keyname = os.path.splitext(os.path.basename(PEM_path))[0]
    ins = ec2.create_instances(ImageId=AMI_ID, MinCount=1,
                               MaxCount=1, KeyName=keyname,
                               InstanceType=insType,
                               TagSpecifications=[{'ResourceType': 'instance',
                                                   'Tags': [{'Key': EC2Tag,
                                                             'Value': tag_value}
                                                            ]}])
    return ins[0].id


def setup_encoder_env(coder_dns, PEM_path):
    print("setting encoder environment")
    ssh_do("/home/ubuntu/server_install_script.sh", coder_dns, PEM_path)
    ssh_do("crontab encoder.cron", coder_dns, PEM_path)


def setup_regular_env(regular_dns, PEM_path):
    print("setting regular environment")
    ssh_do("/home/ubuntu/install_script.sh", regular_dns, PEM_path)


def get_instance_util(cw_client, instance):
    now = datetime.datetime.utcnow()
    start = now - datetime.timedelta(minutes=MEASURETIME)

    request = {"Period" : 300,
               "StartTime" : start,
               "EndTime" : now,
               "MetricName" : "CPUUtilization",
               "Namespace": "AWS/EC2",
               "Statistics" : ["Average"],
               "Dimensions" : [{"Name" :"InstanceId",
                                "Value" : instance}]}

    stat = cw_client.get_metric_statistics(**request)
    if len(stat["Datapoints"]) > 0:
        return stat["Datapoints"][0]["Average"]
    else:
        return 0


class EC2Status:
    def __init__(self, ec2cli):
        self.ec2cli = ec2cli
        self.desc = ec2cli.describe_instances()

    def get_minimoto_instance_worktag(self):
        return {ins["InstanceId"] : parse_tag(ins["Tags"])["workingTag"]
                                        for res in self.desc["Reservations"]
                                        for ins in res["Instances"]
                                        if ins["State"]["Name"] == "running"}


    # {ins_id: (status, util)}
    def get_encoder_status_util(self, cwcli):
        ec2status = self.get_minimoto_instance_worktag()
        ec2util = {ins: get_instance_util(cwcli,ins) for ins in ec2status.keys()}
        return {ins:(ec2status[ins], ec2util[ins]) for ins in ec2status
                                if ec2status[ins] in ["running", "stopped"]}

    def get_num_running(self):
        cwcli = boto3.client("cloudwatch")
        stat = self.get_encoder_status_util(cwcli)
        return len([stat[x] for x in stat
                      if stat[x][0] == "running"])

    def get_num_encoder(self):
        cwcli = boto3.client("cloudwatch")
        stat = self.get_encoder_status_util(cwcli)
        return len([stat[x] for x in stat
                      if stat[x][0] in ["running", "stopped"]])

    def get_running_list(self):
        cwcli = boto3.client("cloudwatch")
        stat = self.get_encoder_status_util(cwcli)
        return [stat[x] for x in stat
                      if stat[x][0] == "running"]


    def get_stopped_list(self):
        cwcli = boto3.client("cloudwatch")
        stat = self.get_encoder_status_util(cwcli)
        return [stat[x] for x in stat
                      if stat[x][0] == "stopped"]

    def get_num_stopped(self):
        return len(self.get_stopped_list())

    def get_stopped_list(self):
        cwcli = boto3.client("cloudwatch")
        stat = self.get_encoder_status_util(cwcli)
        return [ins for ins in stat
                          if stat[ins][0] == "stopped"]

    def get_stopped_instance(self):
        return [ins["InstanceId"] for res in self.desc["Reservations"]
                                  for ins in res["Instances"]
                                  if ins["State"]["Name"] == "stopped"]

    def get_running_encoder(self):
        instag = self.get_minimoto_instance_worktag()
        return [ins["InstanceId"] for res in self.desc["Reservations"]
                                  for ins in res["Instances"]
                                  if ins["State"]["Name"] == "running" and
                                  instag[ins["InstanceId"]] == "running"]

    def get_running_encoder_instance(self):
        instag = self.get_minimoto_instance_worktag()
        return [ins["InstanceId"] for res in self.desc["Reservations"]
                                  for ins in res["Instances"]
                                  if ins["State"]["Name"] == "running" and
                                  instag[ins["InstanceId"]] in ["running", "stopped"]]


    def get_running_instance(self):
        return [ins["InstanceId"] for res in self.desc["Reservations"]
                                  for ins in res["Instances"]
                                  if ins["State"]["Name"] == "running"]

    ''' Return a list of instance id belongs to minimoto'''
    def get_minimoto_instances(self):
        ec2status = {ins["InstanceId"] : parse_tag(ins["Tags"])["workingTag"]
                                        for res in self.desc["Reservations"]
                                        for ins in res["Instances"]
                                        if ins["State"]["Name"] == "running"}
        return [ins for ins in ec2status
                    if ec2status[ins] in ["running", "stopped", "client", "watchdog"]]

    def get_instance_dns(self):
        return {ins["InstanceId"]:ins["PublicDnsName"]
                              for res in self.desc["Reservations"]
                              for ins in res["Instances"]
                              if ins["State"]["Name"] == "running"}

    def get_instance_secure_group_id(self):
        return {ins["InstanceId"]:ins["SecurityGroups"]
                              for res in self.desc["Reservations"]
                              for ins in res["Instances"]
                              if ins["State"]["Name"] == "running"}

